<?php

require __DIR__.'/vendor/autoload.php';

use Symfony\Bundle\FrameworkBundle\Console\Application;
use AppBundle\Command\CommissionCommand;

require __DIR__.'/app/AppKernel.php';

$kernel = new AppKernel('dev', true);

$application = new Application($kernel);

// register commands
$application->add(new CommissionCommand());

$application->run();
