<?php

/* @Twig/Exception/traces.txt.twig */
class __TwigTemplate_23a0e2c3376f5dc271f59d1eb34dcde1b065543218c9d7ee5373421b4ba2781d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0b2feb200725f992bb73d3780d8796f7cfa2092317ec38d1a18f6d8c3c75f701 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0b2feb200725f992bb73d3780d8796f7cfa2092317ec38d1a18f6d8c3c75f701->enter($__internal_0b2feb200725f992bb73d3780d8796f7cfa2092317ec38d1a18f6d8c3c75f701_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/traces.txt.twig"));

        $__internal_492822730b4d70a581d683abf768147bc358d0e12c428705b49a4627634b984e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_492822730b4d70a581d683abf768147bc358d0e12c428705b49a4627634b984e->enter($__internal_492822730b4d70a581d683abf768147bc358d0e12c428705b49a4627634b984e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/traces.txt.twig"));

        // line 1
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["exception"] ?? null), "trace", array()))) {
            // line 2
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), ($context["exception"] ?? null), "trace", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["trace"]) {
                // line 3
                $this->loadTemplate("@Twig/Exception/trace.txt.twig", "@Twig/Exception/traces.txt.twig", 3)->display(array("trace" => $context["trace"]));
                // line 4
                echo "
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['trace'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        
        $__internal_0b2feb200725f992bb73d3780d8796f7cfa2092317ec38d1a18f6d8c3c75f701->leave($__internal_0b2feb200725f992bb73d3780d8796f7cfa2092317ec38d1a18f6d8c3c75f701_prof);

        
        $__internal_492822730b4d70a581d683abf768147bc358d0e12c428705b49a4627634b984e->leave($__internal_492822730b4d70a581d683abf768147bc358d0e12c428705b49a4627634b984e_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/traces.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 4,  31 => 3,  27 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if exception.trace|length %}
{% for trace in exception.trace %}
{% include '@Twig/Exception/trace.txt.twig' with { 'trace': trace } only %}

{% endfor %}
{% endif %}
", "@Twig/Exception/traces.txt.twig", "/home/programuotojas/projects/task/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/traces.txt.twig");
    }
}
