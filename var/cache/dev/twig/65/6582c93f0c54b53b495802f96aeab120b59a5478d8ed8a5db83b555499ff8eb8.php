<?php

/* @Twig/Exception/trace.txt.twig */
class __TwigTemplate_388b710ad6cf498cd261f48ddeaf63e1620c9c042a51d37a4ed4034e8dff645d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_40537086b1497fd82b15d17173b83966d422c11bbb41c2e83dbe3ec132369c38 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_40537086b1497fd82b15d17173b83966d422c11bbb41c2e83dbe3ec132369c38->enter($__internal_40537086b1497fd82b15d17173b83966d422c11bbb41c2e83dbe3ec132369c38_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/trace.txt.twig"));

        $__internal_a187dd4e085736fde93c4f6e6f7f77ce322f53ab690c2201b8a0e1b199975fc8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a187dd4e085736fde93c4f6e6f7f77ce322f53ab690c2201b8a0e1b199975fc8->enter($__internal_a187dd4e085736fde93c4f6e6f7f77ce322f53ab690c2201b8a0e1b199975fc8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/trace.txt.twig"));

        // line 1
        if (twig_get_attribute($this->env, $this->getSourceContext(), ($context["trace"] ?? null), "function", array())) {
            // line 2
            echo "    at ";
            echo ((twig_get_attribute($this->env, $this->getSourceContext(), ($context["trace"] ?? null), "class", array()) . twig_get_attribute($this->env, $this->getSourceContext(), ($context["trace"] ?? null), "type", array())) . twig_get_attribute($this->env, $this->getSourceContext(), ($context["trace"] ?? null), "function", array()));
            echo "(";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->formatArgsAsText(twig_get_attribute($this->env, $this->getSourceContext(), ($context["trace"] ?? null), "args", array()));
            echo ")
";
        } else {
            // line 4
            echo "    at n/a
";
        }
        // line 6
        if ((twig_get_attribute($this->env, $this->getSourceContext(), ($context["trace"] ?? null), "file", array(), "any", true, true) && twig_get_attribute($this->env, $this->getSourceContext(), ($context["trace"] ?? null), "line", array(), "any", true, true))) {
            // line 7
            echo "        in ";
            echo twig_get_attribute($this->env, $this->getSourceContext(), ($context["trace"] ?? null), "file", array());
            echo " line ";
            echo twig_get_attribute($this->env, $this->getSourceContext(), ($context["trace"] ?? null), "line", array());
            echo "
";
        }
        
        $__internal_40537086b1497fd82b15d17173b83966d422c11bbb41c2e83dbe3ec132369c38->leave($__internal_40537086b1497fd82b15d17173b83966d422c11bbb41c2e83dbe3ec132369c38_prof);

        
        $__internal_a187dd4e085736fde93c4f6e6f7f77ce322f53ab690c2201b8a0e1b199975fc8->leave($__internal_a187dd4e085736fde93c4f6e6f7f77ce322f53ab690c2201b8a0e1b199975fc8_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/trace.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 7,  39 => 6,  35 => 4,  27 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if trace.function %}
    at {{ trace.class ~ trace.type ~ trace.function }}({{ trace.args|format_args_as_text }})
{% else %}
    at n/a
{% endif %}
{% if trace.file is defined and trace.line is defined %}
        in {{ trace.file }} line {{ trace.line }}
{% endif %}
", "@Twig/Exception/trace.txt.twig", "/home/programuotojas/projects/task/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/trace.txt.twig");
    }
}
