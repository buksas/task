<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_4c74e5f42d924329df6451cc4712f996fcdf1110a89ae1ac8278fa4ee1d7e59c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_beb868b29b4d8fd48a022d659df0d86a25dccff90e4cd0cbd1a6161f5eae41be = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_beb868b29b4d8fd48a022d659df0d86a25dccff90e4cd0cbd1a6161f5eae41be->enter($__internal_beb868b29b4d8fd48a022d659df0d86a25dccff90e4cd0cbd1a6161f5eae41be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_2e0be02e03e2826c5e4a3d38ccf7e4a80c3b34f47e869bcae71a74451f475897 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2e0be02e03e2826c5e4a3d38ccf7e4a80c3b34f47e869bcae71a74451f475897->enter($__internal_2e0be02e03e2826c5e4a3d38ccf7e4a80c3b34f47e869bcae71a74451f475897_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_beb868b29b4d8fd48a022d659df0d86a25dccff90e4cd0cbd1a6161f5eae41be->leave($__internal_beb868b29b4d8fd48a022d659df0d86a25dccff90e4cd0cbd1a6161f5eae41be_prof);

        
        $__internal_2e0be02e03e2826c5e4a3d38ccf7e4a80c3b34f47e869bcae71a74451f475897->leave($__internal_2e0be02e03e2826c5e4a3d38ccf7e4a80c3b34f47e869bcae71a74451f475897_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_1aa5e730b83228cc9c66ad8863ffa4ff7ea4e0b3eb3984353c6a9e13176567ef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1aa5e730b83228cc9c66ad8863ffa4ff7ea4e0b3eb3984353c6a9e13176567ef->enter($__internal_1aa5e730b83228cc9c66ad8863ffa4ff7ea4e0b3eb3984353c6a9e13176567ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_710f2ea0c98dfa1d40570441105161491110fe42399766d0763526bf69b3b31c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_710f2ea0c98dfa1d40570441105161491110fe42399766d0763526bf69b3b31c->enter($__internal_710f2ea0c98dfa1d40570441105161491110fe42399766d0763526bf69b3b31c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_710f2ea0c98dfa1d40570441105161491110fe42399766d0763526bf69b3b31c->leave($__internal_710f2ea0c98dfa1d40570441105161491110fe42399766d0763526bf69b3b31c_prof);

        
        $__internal_1aa5e730b83228cc9c66ad8863ffa4ff7ea4e0b3eb3984353c6a9e13176567ef->leave($__internal_1aa5e730b83228cc9c66ad8863ffa4ff7ea4e0b3eb3984353c6a9e13176567ef_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_d6481a6e71409996320edf5c1dcbc6a7e233fac0565d9ffc08689b1cea058fce = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d6481a6e71409996320edf5c1dcbc6a7e233fac0565d9ffc08689b1cea058fce->enter($__internal_d6481a6e71409996320edf5c1dcbc6a7e233fac0565d9ffc08689b1cea058fce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_36ab306b69bf8a3c2e06b959810408f68f4fb063d97f59c893646f7624be437e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_36ab306b69bf8a3c2e06b959810408f68f4fb063d97f59c893646f7624be437e->enter($__internal_36ab306b69bf8a3c2e06b959810408f68f4fb063d97f59c893646f7624be437e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["exception"] ?? null), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? null), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? null), "html", null, true);
        echo ")
";
        
        $__internal_36ab306b69bf8a3c2e06b959810408f68f4fb063d97f59c893646f7624be437e->leave($__internal_36ab306b69bf8a3c2e06b959810408f68f4fb063d97f59c893646f7624be437e_prof);

        
        $__internal_d6481a6e71409996320edf5c1dcbc6a7e233fac0565d9ffc08689b1cea058fce->leave($__internal_d6481a6e71409996320edf5c1dcbc6a7e233fac0565d9ffc08689b1cea058fce_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_f4128838c4762f2dbfabb7c49648d9e958879c22c08794e0129c83e0e0e1fb68 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f4128838c4762f2dbfabb7c49648d9e958879c22c08794e0129c83e0e0e1fb68->enter($__internal_f4128838c4762f2dbfabb7c49648d9e958879c22c08794e0129c83e0e0e1fb68_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_940683aa79c65d5440f78a8ba1e1e30ccf9450fcb266ad6f6597d54e1877ef47 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_940683aa79c65d5440f78a8ba1e1e30ccf9450fcb266ad6f6597d54e1877ef47->enter($__internal_940683aa79c65d5440f78a8ba1e1e30ccf9450fcb266ad6f6597d54e1877ef47_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_940683aa79c65d5440f78a8ba1e1e30ccf9450fcb266ad6f6597d54e1877ef47->leave($__internal_940683aa79c65d5440f78a8ba1e1e30ccf9450fcb266ad6f6597d54e1877ef47_prof);

        
        $__internal_f4128838c4762f2dbfabb7c49648d9e958879c22c08794e0129c83e0e0e1fb68->leave($__internal_f4128838c4762f2dbfabb7c49648d9e958879c22c08794e0129c83e0e0e1fb68_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/home/programuotojas/projects/task/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
