<?php

namespace AppBundle\Services;

/**
 * Class Calculator
 * @package AppBundle\Services
 */
/**
 * Class Calculator
 * @package AppBundle\Services
 */
/**
 * Class Calculator
 * @package AppBundle\Services
 */
class Calculator
{
    /**
     * @var array
     */
    private static $PARAMS = [];

    /**
     * Use this array to save natural cash out operations history
     *
     * @var array
     */
    private $discount = [];

    /**
     * Calculator constructor.
     * @param array $params
     */
    public function __construct($params)
    {
        self::$PARAMS = $params;
    }

    /**
     * @param array $data
     * @return array
     */
    public function calculateCommissions($data)
    {
        $result = [];

        foreach ($data as $operation) {
            if ($operation[3] == 'cash_in') {
                // cash in operation
                $result[] = $this->cashIn($operation[4], $operation[5]);
            } else {
                if ($operation[2] == 'legal') {
                    // cash out operation, legal
                    $result[] = $this->cashOutLegal($operation[4], $operation[5]);
                } else {
                    // cash out operation, natural
                    $result[] = $this->cashOutNatural(
                        $this->getDiscount(
                            $operation[0],
                            $operation[1]
                        ),
                        $operation[0],
                        $operation[1],
                        $operation[4],
                        $operation[5]
                    );
                }
            }
        }

        return $result;
    }

    private function getDiscount($date, $client)
    {
        $discount = self::$PARAMS['cash-out-natural-free'];

        if (isset($this->discount[$client])) {
            // it's not the first operation for this client
            $clientDiscount = $this->discount[$client];

            if ($this->isDatesInSameWeek($clientDiscount['date'], $date)) {
                if ($clientDiscount['count'] < self::$PARAMS['cash-out-natural-count']) {
                    $discount = $clientDiscount['amount'];
                } else {
                    // weekly operations limit reached, no more discount
                    $discount = 0.0;
                    $this->setDiscount($client, $date, self::$PARAMS['cash-out-natural-count'], 0.0);
                }
            } else {
                // first operation for this client on the week of $date
                $this->setDiscount($client, $date, $discount, 0);
            }
        } else {
            // first operation for this client
            $this->setDiscount($client, $date, $discount, 0);
        }

        return $discount;
    }

    private function setDiscount($client, $date, $amount, $count = null)
    {
        $newData = [
            'date' => $date,
            'count' => is_null($count) ? $this->discount[$client]['count'] + 1 : $count,
            'amount' => $amount,
        ];

        $this->discount[$client] = $newData;
    }

    private function isDatesInSameWeek($dateString, $currentDateString)
    {
        $monday = date("Y-m-d", strtotime('monday this week', strtotime($dateString)));
        $sunday = date("Y-m-d", strtotime('sunday this week', strtotime($dateString)));
        $date = date($currentDateString);

        return $date >= $monday && $date <= $sunday;
    }

    private function cashOutNatural($discount, $date, $client, $amount, $currencyName)
    {
        $currency = self::$PARAMS['currencies'][$currencyName];
        $commission = 0.0;

        // convert discount to given currency
        $discount = $discount * $currency['rate'];

        if ($amount > $discount) {
            $commission = ($amount - $discount) * self::$PARAMS['cash-out'];
            // no more discount this week for the client after this operation
            $this->setDiscount(
                $client,
                $date,
                0.0
            );
        } else {
            // save discount in default currency
            $this->setDiscount(
                $client,
                $date,
                $this->round(($discount - $amount) / $currency['rate'], $currency['round'])
            );
        }

        return $this->round($commission, $currency['round']);
    }

    /**
     * @param $amount
     * @param $currencyName
     * @return float|int
     */
    private function cashOutLegal($amount, $currencyName)
    {
        $min = self::$PARAMS['cash-out-legal-min'];
        $currency = self::$PARAMS['currencies'][$currencyName];
        $commission = $amount * self::$PARAMS['cash-out'];

        // convert min commission fee to given currency
        $min = $min * $currency['rate'];

        // commission fee can't be less than min value
        if ($commission < $min) {
            $commission = $min;
        }

        return $this->round($commission, $currency['round']);
    }

    /**
     * @param $amount
     * @param $currencyName
     * @return float|int
     */
    private function cashIn($amount, $currencyName)
    {
        $max = self::$PARAMS['cash-in-max'];
        $currency = self::$PARAMS['currencies'][$currencyName];
        $commission = $amount * self::$PARAMS['cash-in'];

        // convert max commission fee to given currency
        $max = $max * $currency['rate'];

        // commission fee can't exceed max value
        if ($commission > $max) {
            $commission = $max;
        }

        return $this->round($commission, $currency['round']);
    }

    /**
     * @param $commission
     * @param $smallestAmount
     * @return float|int
     */
    private function round($commission, $smallestAmount)
    {
        // convert to smallest units of given currency to round and back to normal currency value
        return ceil($commission * $smallestAmount) / $smallestAmount;
    }
}
