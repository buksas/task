<?php
/**
 * Created by PhpStorm.
 * User: programuotojas
 * Date: 17.3.4
 * Time: 16.10
 */

namespace AppBundle\Services;
use Symfony\Component\Finder\Finder;

/**
 * Class FileParser
 * @package AppBundle\Services
 */
class FileParser
{
    /**
     * @param string $filename
     * @return string|array
     */
    public function parse($filename)
    {
        return array_map('str_getcsv', file($filename));
    }
}
