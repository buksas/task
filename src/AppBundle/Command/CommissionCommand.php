<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Class CommissionCommand
 * @package AppBundle\Command
 */
class CommissionCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('calculate')

            // the short description shown while running "php bin/console list"
            ->setDescription('Calculates a commission fee.')

            // configure an argument
            ->addArgument('filename', InputArgument::REQUIRED, 'The name of data file.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $parser = $this->getContainer()->get('app.parser');
        $calculator = $this->getContainer()->get('app.calculator');
        $commissions = [];
        $data = $parser->parse(
            $input->getArgument('filename')
        );

        if (is_array($data) && count($data) > 0) {
            $commissions = $calculator->calculateCommissions($data);
        }

        $output->writeln($commissions);
    }
}
